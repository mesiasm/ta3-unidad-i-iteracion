"""                     author: "Angel Morocho"
               email: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 2:Escribe otro programa que pida una lista de números como la anterior
            y al final muestre por pantalla el máximo y mínimo de los números,
             en vez de la media """


cont_cad = 1
num_ing = []
contador = 1
mayor = 0
menor = 0

while True:
    try:
        cant = input("Ingresa la cantiddad de numeros ")
        cantidad = int(cant)
        while contador <= cantidad:
            try:
                ing = input("Ingresa el número  " + str(cont_cad) + ":")
                numero = float(ing)
                num_ing.append(numero)
                contador = contador + 1
                cont_cad += 1
                mayor = max(num_ing)
                menor = min(num_ing)

            except ValueError:
                if ing.lower() in ["fin"]:
                    break
                print("Valor incorrecto. Intenta nuevamente")
        print("La cantidad de numeros inresados son:", contador - 1)
        print("los numeros ingresados son: ", num_ing)
        print("El número mayor es: ", mayor)
        print("El número menor es: ", menor)
        break
    except ValueError:
        if cant.lower() in ["fin"]:
            break
        print("Valor incorrecto. Intenta nuevamente")