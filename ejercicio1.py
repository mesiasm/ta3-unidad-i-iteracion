"""                         author: "Angel Morocho"
                    email: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 1: Escribe un programa que lea repetidamente números hasta que el
             usuario introduzca “fin”. Un vez se haya introducido “fin”, muestra
             por pantalla el total, la cantidad de números y la media de esos números.
             Si el usuario introduce cualquier otra cosa que no sea un número,
             detecta su fallo usando try y except, muestra un mensaje de error y pasa
             al número siguiente """

contador = 0
total = 0
while True:

    try:
        numero = input("Ingresa un número ")
        total += int(numero)
        contador += 1 

    except ValueError:
        if numero.lower() in ["fin"]:
            break
        print("Valor incorrecto. Intenta nuevamente")

if contador == 0:
    promedio = 'No se puede dividir para cero'
else:
    promedio = total/contador
print("La suma de los números es de: ", total)
print("La cantidad de números introducidos es de: ", contador)
print("El promedio de valores ha sido de:  ", promedio)